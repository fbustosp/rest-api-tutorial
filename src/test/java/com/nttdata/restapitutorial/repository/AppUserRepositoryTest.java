package com.nttdata.restapitutorial.repository;

import com.nttdata.restapitutorial.model.AppUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class AppUserRepositoryTest {

    @Autowired
    private AppUserRepository appUserRepository;

    private AppUser userOne;
    private AppUser userTwo;

    @BeforeEach
    void setUp() {
        userOne = new AppUser(
                "circaansfbp",
                "fran@gmail.com",
                "12345678"
        );
        userTwo = new AppUser(
                "ignaico",
                "ignacio@gmail.com",
                "12345"
        );

        appUserRepository.save(userOne);
        appUserRepository.save(userTwo);
    }

    @AfterEach
    void tearDown() {
        appUserRepository.deleteAll();
    }

    // Method: findByUsername()
    @Test
    @DisplayName("It finds a user with the username passed as a parameter and returns it")
    void itReturnsAUserWhoseUsernameMatchesTheOnePassedAsParameter() {
        // Arrange
        String username = "circaansfbp";

        // Act
        AppUser userFound = appUserRepository.findByUsername(username).get();

        // Assert
        assertEquals(userOne, userFound);
    }

    @Test
    @DisplayName("It doesnt find a user with the username provided, so it returns an Optional with no value")
    void itDoesntFindAUserWithTheUsernamePassedAsParameter() {
        // Arrange
        String username = "mouse";

        // Act
        Optional<AppUser> userFound = appUserRepository.findByUsername(username);

        // Assert
        assertTrue(userFound.isEmpty());
    }

    // Method: existsByUsername()
    @Test
    @DisplayName("It finds a user that matches the username given so it returns true")
    void itFindsAUserThatMatchesTheUsernameGivenSoItReturnsTrue() {
        // Arrange
        String username = "ignaico";

        // Act
        boolean isUserFound = appUserRepository.existsByUsername(username);

        // Assert
        assertTrue(isUserFound);
    }

    @Test
    @DisplayName("It doesnt find a user whose username matches the one given so it returns false")
    void itDoesntFindAUserThatMatchesTheUsernameGivenSoItReturnsFalse() {
        // Arrange
        String username = "username";

        // Act
        boolean isUserFound = appUserRepository.existsByUsername(username);

        // Assert
        assertFalse(isUserFound);
    }

    // Method: existsByEmail()
    @Test
    @DisplayName("It finds a user with the email given so it returns true")
    void itFindsUserWithEmailGivenSoReturnsTrue() {
        // Arrange
        String email = "ignacio@gmail.com";

        // Act
        boolean isUserFound = appUserRepository.existsByEmail(email);

        // Assert
        assertTrue(isUserFound);
    }

    @Test
    @DisplayName("It doesnt find a user with the email provided so it returns false")
    void itDoesntFindUserWithEmailProvidedSoReturnsFalse() {
        // Arrange
        String email = "default@outlook.com";

        // Act
        boolean isUserFound = appUserRepository.existsByEmail(email);

        // Assert
        assertFalse(isUserFound);
    }
}