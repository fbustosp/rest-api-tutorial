package com.nttdata.restapitutorial.repository;

import com.nttdata.restapitutorial.model.Tutorial;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TutorialRepositoryTest {

    @Autowired
    private TutorialRepository tutorialRepository;

    private Tutorial tutorialOne;
    private Tutorial tutorialTwo;

    // Setup inicial
    @BeforeEach
    void setUp() {
        tutorialOne = new Tutorial(
                "Spring Boot REST API tutorial",
                "Learn how to build your own REST applications using Spring Boot",
                false
        );

        tutorialTwo = new Tutorial(
                "Spring Boot JPA tutorial",
                "Learn all about Spring Data JPA!",
                true
        );

        tutorialRepository.save(tutorialOne);
        tutorialRepository.save(tutorialTwo);
    }

    // Borra los registros luego de realizar un test, para mantener una BD limpia
    @AfterEach
    void tearDown() {
        tutorialRepository.deleteAll();
    }

    // method: findByPublished()
    @Test
    @DisplayName("There is a tutorial published, so it finds it and returns it")
    void itFindsATutorialPublishedSoItIsReturned() {
        // Act
        List<Tutorial> tutorialsFound = tutorialRepository.findByPublished(true);

        // Assert
        assertNotNull(tutorialsFound);
        assertEquals(1, tutorialsFound.size());
    }

    @Test
    @DisplayName("There are no published tutorials so it returns an empty list")
    void itDoesntFindPublishedTutorialsSoItReturnsEmptyList() {
        // Arrange
        tutorialTwo.setPublished(false);
        tutorialRepository.save(tutorialTwo);

        // Act
        List<Tutorial> tutorialsFound = tutorialRepository.findByPublished(true);

        // Assert
        assertThat(tutorialsFound).isEmpty();
        assertEquals(0, tutorialsFound.size());
    }

    // method: findByTitleContaining()
    @Test
    @DisplayName("It finds a tutorial with a title that matches the word given as a parameter")
    void returnsATutorialWithATitleThatMatchesTheStringPassedAsParameter() {
        // Arrange
        String wordInTitle = "REST";

        // Act
        List<Tutorial> tutorialsFound = tutorialRepository.findByTitleContaining(wordInTitle);

        // Assert
        assertEquals(1, tutorialsFound.size());
        assertEquals(tutorialOne, tutorialsFound.get(0));
        assertThat(tutorialsFound).isNotEmpty();
    }

    @Test
    @DisplayName("There are no tutorials with a title that matches the word given as a parameter")
    void noTutorialsFoundWithWordPassedAsMethodParameter() {
        // Arrange
        String wordInTitle = "word";

        // Act
        List<Tutorial> tutorialsFound = tutorialRepository.findByTitleContaining(wordInTitle);

        // Assert
        assertThat(tutorialsFound).isEmpty();
        assertEquals(0, tutorialsFound.size());
    }
}