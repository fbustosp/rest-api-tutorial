package com.nttdata.restapitutorial.repository;

import com.nttdata.restapitutorial.model.Comment;
import com.nttdata.restapitutorial.model.Tutorial;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private TutorialRepository tutorialRepository;

    private Tutorial tutorialOne;
    private Tutorial tutorialTwo;

    private Comment commentOne;
    private Comment commentTwo;

    @BeforeEach
    void setUp() {
        tutorialOne = new Tutorial(
                "First tutorial",
                "This is the description of the first tutorial",
                true
        );
        tutorialTwo = new Tutorial(
                "Second tutorial",
                "This is the description of the second tutorial",
                true
        );

        tutorialRepository.save(tutorialOne);
        tutorialRepository.save(tutorialTwo);

        commentOne = new Comment(
                "This is the content of the first comment",
                tutorialOne
        );
        commentTwo = new Comment(
                "This is the content of the second comment",
                tutorialOne
        );

        commentRepository.save(commentOne);
        commentRepository.save(commentTwo);
    }

    @AfterEach
    void tearDown() {
        commentRepository.deleteAll();
    }

    // Method: findByTutorialId
//    @Test
//    @DisplayName("The tutorial is found and it has comments so those are returned")
//    void tutorialIdFoundAndCommentsReturned() {
//        // Arrange
//        Long tutorialId = 1L;
//
//        // Act
//        List<Comment> commentsFound = commentRepository.findByTutorialId(tutorialId);
//
//        // Assert
//        assertThat(commentsFound).isNotEmpty();
//        assertEquals(2, commentsFound.size());
//    }

    @Test
    @DisplayName("The tutorial is found but it has no comments so an empty list is returned")
    void tutorialFoundWithNoCommentsSoEmptyListIsReturned() {
        // Arrange
        Long tutorialId = 2L;

        // Act
        List<Comment> commentsFound = commentRepository.findByTutorialId(tutorialId);

        // Assert
        assertThat(commentsFound).isEmpty();
        assertEquals(0, commentsFound.size());
    }
}