package com.nttdata.restapitutorial.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.restapitutorial.model.Comment;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.service.impl.CommentService;
import com.nttdata.restapitutorial.service.impl.TutorialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
class CommentRestControllerTest {

    private MockMvc mockMvc;

    private JacksonTester<List<Comment>> jsonListComments;
    private JacksonTester<Comment> jsonComment;

    @Mock
    private CommentService commentService;

    @Mock
    private TutorialService tutorialService;

    @InjectMocks
    private CommentRestController commentRestController;

    private Tutorial firstTutorial;
    private Tutorial secondTutorial;

    private Comment firstComment;
    private Comment secondComment;

    @BeforeEach
    void setUp() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(commentRestController).build();

        firstTutorial = new Tutorial(
                "This is the first tutorial",
                "This is the description of the first tutorial",
                false
        );
        secondTutorial = new Tutorial(
                "This is the second tutorial",
                "This is the description of the second tutorial",
                true
        );

        firstComment = new Comment(
                "This is the content of the first comment",
                firstTutorial
        );
        secondComment = new Comment(
                "This is the content of the second comment",
                firstTutorial
        );
    }

    // Method: getCommentsByTutorialId()
    @Test
    @DisplayName("It finds a tutorial with comments in it and returns a list of them with status 200 OK")
    void returnsListOfCommentsOfTutorialWithStatusOk() throws Exception {
        // Arrange
        Long tutorialId = 1L;

        List<Comment> commentsFound = new ArrayList<>();
        commentsFound.add(firstComment);
        commentsFound.add(secondComment);

        when(commentService.getCommentsByTutorialId(tutorialId)).thenReturn(commentsFound);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials/{tutorial_id}/comments", tutorialId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonListComments.write(commentsFound).getJson(), response.getContentAsString());
        verify(commentService, times(1)).getCommentsByTutorialId(tutorialId);
    }

    @Test
    @DisplayName("There are no comments on the tutorial so it returns status 204 NO_CONTENT")
    void noCommentsFoundSoItReturnsStatusNoContent() throws Exception {
        // Arrange
        Long tutorialId = 2L;
        List<Comment> noCommentsFound = new ArrayList<>();

        when(commentService.getCommentsByTutorialId(tutorialId)).thenReturn(noCommentsFound);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials/{tutorial_id}/comments", tutorialId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
        verify(commentService, times(1)).getCommentsByTutorialId(tutorialId);
    }

    // Method: getCommentById();
    @Test
    @DisplayName("Returns a comment given a valid ID with a status 200 OK")
    void returnsCommentGivenCorrectIdWithStatusOk() throws Exception {
        // Arrange
        Long commentId = 1L;
        when(commentService.getCommentById(commentId)).thenReturn(firstComment);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/comments/{comment_id}", commentId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonComment.write(firstComment).getJson(), response.getContentAsString());
        verify(commentService, times(1)).getCommentById(commentId);
    }

    // Method: createComment()
    @Test
    @DisplayName("Creates comment successfully and returns it with a status 201 CREATED")
    void createCommentAndReturnsItWithStatusCreated() throws Exception {
        // Arrange
        Long tutorialId = 1L;
        Comment newComment = new Comment(
                "This is the content of the new comment",
                firstTutorial
        );

        when(commentService.createComment(eq(tutorialId), any(Comment.class))).thenReturn(newComment);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/tutorials/{tutorial_id}/comments", tutorialId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonComment.write(newComment).getJson())
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals(jsonComment.write(newComment).getJson(), response.getContentAsString());
        verify(commentService, times(1)).createComment(eq(tutorialId), any(Comment.class));
    }

    // Method: updateComment()
    @Test
    @DisplayName("Updates a comment successfully and returns it with status 202 ACCEPTED")
    void updatesCommentSuccessfullyAndReturnsItWithStatusAccepted() throws Exception {
        // Arrange
        Long commentId = 2L;
        Comment commentWithUpdatedContent = new Comment(
                "This content has been updated",
                firstTutorial
        );

        when(commentService.updateComment(eq(commentId), any(Comment.class))).thenReturn(commentWithUpdatedContent);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/comments/{comment_id}", commentId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonComment.write(commentWithUpdatedContent).getJson())
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        assertEquals(jsonComment.write(commentWithUpdatedContent).getJson(), response.getContentAsString());
        verify(commentService, times(1)).updateComment(eq(commentId), any(Comment.class));
    }

    // Method: deleteComment()
    @Test
    @DisplayName("Deletes the comment successfully and returns status 202 ACCEPTED")
    void deletesCommentSuccessfullyAndReturnsStatusAccepted() throws Exception {
        // Arrange
        Long commentId = 2L;
        doNothing().when(commentService).deleteComment(commentId);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                delete("/api/comments/{comment_id}", commentId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        verify(commentService, times(1)).deleteComment(commentId);
    }

    // Method: deleteAllCommentsOfTutorial()
    @Test
    @DisplayName("Deletes all comments from a tutorial successfully and returns status 202 ACCEPTED")
    void deletesAllCommentsFromTutorialAndReturnsStatusAccepted() throws Exception {
        // Arrange
        Long tutorialId = 1L;
        doNothing().when(commentService).deleteAllCommentsOfTutorial(tutorialId);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                delete("/api/tutorials/{tutorial_id}/comments", tutorialId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        verify(commentService, times(1)).deleteAllCommentsOfTutorial(tutorialId);
    }
}