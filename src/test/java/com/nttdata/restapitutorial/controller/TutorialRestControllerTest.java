package com.nttdata.restapitutorial.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.service.impl.TutorialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
class TutorialRestControllerTest {

    private MockMvc mockMvc;

    private JacksonTester<Tutorial> jsonTutorial;
    private JacksonTester<List<Tutorial>> jsonTutorialsList;

    @Mock
    private TutorialService tutorialService;

    @InjectMocks
    private TutorialRestController tutorialRestController;

    private Tutorial firstTutorial;
    private Tutorial secondTutorial;
    private Tutorial thirdTutorial;
    private List<Tutorial> tutorials;

    @BeforeEach
    void setUp() {
        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders.standaloneSetup(tutorialRestController).build();

        firstTutorial = new Tutorial(
                "Spring Boot REST tutorial #1",
                "Description of the first tutorial",
                false
        );
        secondTutorial = new Tutorial(
                "Spring Data JPA tutorial",
                "Description of the second tutorial",
                false
        );
        thirdTutorial = new Tutorial(
                "Spring Boot REST API Tutorial",
                "Description of the third tutorial",
                true
        );

        tutorials = new ArrayList<>();
        tutorials.add(firstTutorial);
        tutorials.add(secondTutorial);
        tutorials.add(thirdTutorial);
    }

    // Method: getAllTutorials()
    @Test
    @DisplayName("Returns all tutorials successfully with a status 200 OK")
    void returnsAllTutorials() throws Exception {
        // Arrange
        when(tutorialService.getAllTutorials()).thenReturn(tutorials);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonTutorialsList.write(tutorials).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).getAllTutorials();
    }

    @Test
    @DisplayName("Returns all tutorials that match the parameter given with a status 200 OK")
    void returnsAllTutorialsThatMatchTheQueryParameterGiven() throws Exception {
        // Arrange
        String param = "REST";

        List<Tutorial> expectedTutorials = new ArrayList<>();
        expectedTutorials.add(firstTutorial);
        expectedTutorials.add(thirdTutorial);

        when(tutorialService.findByTitleContaining(param)).thenReturn(expectedTutorials);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials").param("title", param).accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonTutorialsList.write(expectedTutorials).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).findByTitleContaining(param);
    }

    @Test
    @DisplayName("Returns an empty list and a status 204 No Content given a parameter that doesnt match any tutorial title")
    void returnsEmptyListGivenParameterThatDoesntMatchAnyTutorialTitle() throws Exception {
        // Arrange
        String param = "word";
        List<Tutorial> noContentList = new ArrayList<>();

        when(tutorialService.findByTitleContaining(param)).thenReturn(noContentList);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials").param("title", param).accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
        verify(tutorialService, times(1)).findByTitleContaining(param);
    }

    // method: getTutorialById()
    @Test
    @DisplayName("Finds and returns the tutorial given a correct ID with a status 200 OK")
    void returnsTutorialCorrectlyWithStatusOk() throws Exception {
        // Arrange
        Long tutorialId = 2L;
        when(tutorialService.getTutorialById(tutorialId)).thenReturn(secondTutorial);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials/{tutorial_id}", tutorialId).accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonTutorial.write(secondTutorial).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).getTutorialById(tutorialId);
    }

    // Method: createTutorial()
    @Test
    @DisplayName("Saves the new tutorial successfully and returns it with a status 201 Created")
    void createsTutorialCorrectlyAndReturnsItWithStatusCreated() throws Exception {
        // Arrange
        Tutorial newTutorial = new Tutorial(
                "Spring JPA tutorial 2",
                "Description of this tutorial",
                true
        );
        when(tutorialService.createTutorial(any(Tutorial.class))).thenReturn(newTutorial);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                post("/api/tutorials")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonTutorial.write(newTutorial).getJson())
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals(jsonTutorial.write(newTutorial).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).createTutorial(any(Tutorial.class));
    }

    // Method: updateTutorial()
    @Test
    @DisplayName("Updates the tutorial successfully and returns a status 202 Accepted")
    void updateTutorialSuccessfullyAndReturnsStatusAccepted() throws Exception {
        // Arrange
        Long tutorialId = 1L;
        Tutorial updatedContent = new Tutorial(
                "The title has been updated",
                "As well as the description",
                true
        );

        when(tutorialService.updateTutorial(eq(tutorialId), any(Tutorial.class))).thenReturn(updatedContent);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                put("/api/tutorials/{tutorial_id}", tutorialId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonTutorial.write(updatedContent).getJson())
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        assertEquals(jsonTutorial.write(updatedContent).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).updateTutorial(eq(tutorialId), any(Tutorial.class));
    }

    // Method: deleteTutorial()
    @Test
    @DisplayName("Deletes a tutorial successfully and returns status 202 Accepted")
    void deletesTutorialAndReturnsAcceptedStatus() throws Exception {
        // Arrange
        Long tutorialId = 1L;
        doNothing().when(tutorialService).deleteTutorial(tutorialId);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                delete("/api/tutorials/{tutorial_id}", tutorialId)
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        verify(tutorialService, times(1)).deleteTutorial(tutorialId);
    }

    // method: deleteAllTutorials()
    @Test
    @DisplayName("Deletes all of the tutorials successfully and returns status 201 Accepted")
    void deletesAllTutorialsSuccessfullyAndReturnsStatusOk() throws Exception {
        // Arrange
        doNothing().when(tutorialService).deleteAllTutorials();

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                delete("/api/tutorials")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatus());
        verify(tutorialService, times(1)).deleteAllTutorials();
    }

    // method: findByPublished()
    @Test
    @DisplayName("It finds published tutorials and returns them with a status 200 OK")
    void itFindsPublishedTutorialsAndReturnsThemWithStatusOk() throws Exception {
        // Arrange
        List<Tutorial> publishedTutorials = new ArrayList<>();
        publishedTutorials.add(thirdTutorial);

        when(tutorialService.findByPublished()).thenReturn(publishedTutorials);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials/published")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonTutorialsList.write(publishedTutorials).getJson(), response.getContentAsString());
        verify(tutorialService, times(1)).findByPublished();
    }

    @Test
    @DisplayName("There are no published tutorials so it returns a status 204 No Content")
    void thereAreNoPublishedTutorialsSoItReturnsStatusNoContent() throws Exception {
        // Arrange
        List<Tutorial> noPublishedTutorials = new ArrayList<>();
        when(tutorialService.findByPublished()).thenReturn(noPublishedTutorials);

        // Act
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/tutorials/published")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // Assert
        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
        verify(tutorialService, times(1)).findByPublished();
    }
}