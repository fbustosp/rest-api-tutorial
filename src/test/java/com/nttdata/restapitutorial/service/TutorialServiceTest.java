package com.nttdata.restapitutorial.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.nttdata.restapitutorial.exception.ResourceNotFoundException;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.repository.TutorialRepository;
import com.nttdata.restapitutorial.service.impl.TutorialService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TutorialServiceTest {

    @Mock
    private TutorialRepository tutorialRepository;

    @InjectMocks
    private TutorialService tutorialService;

    private static Tutorial firstTutorial;
    private static Tutorial secondTutorial;
    private static Tutorial thirdTutorial;
    private static List<Tutorial> tutorials = new ArrayList<>();

    @BeforeAll
    static void setup() {
        firstTutorial = new Tutorial("First tutorial", "This is the first tutorial", false);
        secondTutorial = new Tutorial("Second tutorial", "This is the second tutorial", false);
        thirdTutorial = new Tutorial("Third tutorial", "This is the third tutorial", true);

        tutorials.add(firstTutorial);
        tutorials.add(secondTutorial);
        tutorials.add(thirdTutorial);
    }

    // Retornar todos los tutoriales
    @Test
    void returnsAllTheTutorials() {
        // Arrange
        when(tutorialRepository.findAll()).thenReturn(tutorials);

        // Act
        List<Tutorial> tutorialsFound = tutorialService.getAllTutorials();

        // Assert
        assertEquals(tutorials, tutorialsFound);
        verify(tutorialRepository, times(1)).findAll();
    }

    @Test
    void returnsAllTutorialsThatMatchACertainWordOnItsTitle() {
        // Arrange
        String wordOnTitle = "tutorial";
        when(tutorialRepository.findByTitleContaining(wordOnTitle)).thenReturn(tutorials);

        // Act
        List<Tutorial> tutorialsFound = tutorialService.findByTitleContaining(wordOnTitle);

        // Assert
        assertEquals(tutorials, tutorialsFound);
        verify(tutorialRepository, times(1)).findByTitleContaining(wordOnTitle);
    }

    @Test
    void returnsAnEmptyListWhenTheWordPassedDoesntMatchAnyContainedOnTheTitle() {
        // Arrange
        String wordOnTitle = "title";
        List<Tutorial> emptyTutorialList = new ArrayList<>();
        when(tutorialRepository.findByTitleContaining(wordOnTitle)).thenReturn(emptyTutorialList);

        // Act
        List<Tutorial> tutorialsFound = tutorialService.findByTitleContaining(wordOnTitle);

        // Assert
        assertEquals(0, tutorialsFound.size());
        verify(tutorialRepository, times(1)).findByTitleContaining(wordOnTitle);
    }

    // Obtener un tutorial por ID
    @Test
    void returnsATutorialThatMatchesTheGivenId() {
        // Arrange
        Long tutorialId = 1L;
        when(tutorialRepository.findById(tutorialId)).thenReturn(Optional.of(firstTutorial));

        // Act
        Optional<Tutorial> tutorialFound = Optional.of(tutorialService.getTutorialById(tutorialId));

        // Assert
        assertTrue(tutorialFound.isPresent());
        assertEquals(firstTutorial, tutorialFound.get());
        verify(tutorialRepository, times(1)).findById(tutorialId);
    }

    @Test
    void throwsAnExceptionWhenTheGivenIdDoesntMatchAnyTutorialStoredInTheDatabase() {
        // Arrange
        Long tutorialId = 5L;
        when(tutorialRepository.findById(tutorialId)).thenReturn(Optional.empty());
//        doThrow(ResourceNotFoundException.class).when(tutorialRepository).findById(tutorialId);

        // Act + Assert
        assertThrows(ResourceNotFoundException.class,
                () -> tutorialService.getTutorialById(tutorialId)
        );
    }

    // Creación de un tutorial
    @Test
    void createsNewTutorialCorrectly() {
        // Arrange
        Tutorial newTutorial = new Tutorial("This is the new tutorial", "This tutorial was recently created.", true);
        when(tutorialRepository.save(newTutorial)).thenReturn(newTutorial);

        // Act
        Tutorial createdTutorial = tutorialService.createTutorial(newTutorial);

        // Assert
        assertNotNull(createdTutorial);
        assertAll("createdTutorial",
                () -> assertEquals(newTutorial.getTutorialId(), createdTutorial.getTutorialId()),
                () -> assertEquals(newTutorial.getTitle(), createdTutorial.getTitle()),
                () -> assertEquals(newTutorial.getDescription(), createdTutorial.getDescription()),
                () -> assertEquals(newTutorial.isPublished(), createdTutorial.isPublished()));
        verify(tutorialRepository, times(1)).save(newTutorial);
    }

    // Actualización de un tutorial
    @Test
    void updatesNewTutorialCorrectly() {
        // Arrange
        Long tutorialId = 1L;
        Tutorial tutorialWithUpdatedFields = new Tutorial("The title has been updated", "Also the description", false);
        when(tutorialRepository.findById(tutorialId)).thenReturn(Optional.of(firstTutorial));
        when(tutorialRepository.save(firstTutorial)).thenReturn(firstTutorial);

        // Act
        Tutorial updatedTutorial = tutorialService.updateTutorial(tutorialId, tutorialWithUpdatedFields);

        // Assert
        assertNotNull(updatedTutorial);
        assertAll("updatedTutorial",
                () -> assertEquals(firstTutorial.getTitle(), updatedTutorial.getTitle()),
                () -> assertEquals(firstTutorial.getDescription(), updatedTutorial.getDescription()));
        verify(tutorialRepository, times(1)).save(updatedTutorial);
    }

    // Eliminación de un tutorial
    @Test
    void theTutorialCorrespondingToTheGivenIdIsCorrectlyDeleted() {
        // Arrange
        Long tutorialId = 1L;

        // Act
        tutorialService.deleteTutorial(tutorialId);

        // Assert
        verify(tutorialRepository, times(1)).deleteById(tutorialId);
    }

    // Eliminación de todos los tutoriales
    @Test
    void allTutorialsAreDeleted() {
        // Act
        tutorialService.deleteAllTutorials();

        // Assert
        verify(tutorialRepository, times(1)).deleteAll();
    }

    // Retorna los tutoriales publicados
    @Test
    void returnsAllThePublishedTutorialsCorrectly() {
        // Arrange
        List<Tutorial> publishedTutorials = new ArrayList<>();
        publishedTutorials.add(thirdTutorial);
        when(tutorialRepository.findByPublished(true)).thenReturn(publishedTutorials);

        // Act
        List<Tutorial> tutorialsFound = tutorialService.findByPublished();

        // Assert
        assertEquals(publishedTutorials, tutorialsFound);
        verify(tutorialRepository, times(1)).findByPublished(true);
    }
}
