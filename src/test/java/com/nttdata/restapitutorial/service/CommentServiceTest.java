package com.nttdata.restapitutorial.service;

import com.nttdata.restapitutorial.exception.ResourceNotFoundException;
import com.nttdata.restapitutorial.model.Comment;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.repository.CommentRepository;
import com.nttdata.restapitutorial.repository.TutorialRepository;
import com.nttdata.restapitutorial.service.impl.CommentService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommentServiceTest {

    @Mock
    private CommentRepository commentRepositoryMock;

    @Mock
    private TutorialRepository tutorialRepositoryMock;

    @InjectMocks
    private CommentService commentService;

    // Tutorial
    private static Tutorial firstTutorial;
    private static Tutorial secondTutorial;
    private static Tutorial thirdTutorial;

    // Comment
    private static List<Comment> firstTutorialComments = new ArrayList<>();
    private static List<Comment> secondTutorialComments = new ArrayList<>();
    private static List<Comment> thirdTutorialComments = new ArrayList<>();
    private static Comment firstCommentTutoOne;
    private static Comment secondCommentTutoOne;
    private static Comment thirdCommentTutoTwo;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(commentRepositoryMock);
    }

    @BeforeAll
    static void setup() {
        // Tutorial
        firstTutorial = new Tutorial("First tutorial", "This is the first tutorial", false);
        secondTutorial = new Tutorial("Second tutorial", "This is the second tutorial", false);
        thirdTutorial = new Tutorial("Third tutorial", "This is the third tutorial", true);

        // Comment
        firstCommentTutoOne = new Comment("This is the first comment of the first tutorial", firstTutorial);
        secondCommentTutoOne = new Comment("This is the second comment of the first tutorial", firstTutorial);
        thirdCommentTutoTwo = new Comment("This is the first comment of the second tutorial", secondTutorial);

        firstTutorialComments.add(firstCommentTutoOne);
        firstTutorialComments.add(secondCommentTutoOne);
        secondTutorialComments.add(thirdCommentTutoTwo);
    }

    // Method: getCommentsByTutorialId()
    @Test
    @DisplayName("Returns all the comments given a correct tutorial ID")
    void returnsAllCommentsOfTutorial() {
        // Arrange
        Long tutorialId = 1L;

        when(tutorialRepositoryMock.existsById(tutorialId)).thenReturn(true);
        when(commentRepositoryMock.findByTutorialId(tutorialId)).thenReturn(firstTutorialComments);

        // Act
        List<Comment> foundComments = commentService.getCommentsByTutorialId(tutorialId);

        // Assert
        assertEquals(firstTutorialComments, foundComments);
        assertEquals(2, foundComments.size());
        verify(tutorialRepositoryMock, times(1)).existsById(tutorialId);
        verify(commentRepositoryMock, times(1)).findByTutorialId(tutorialId);
    }

    @Test
    @DisplayName("Throws ResourceNotFoundException when given an invalid tutorial ID")
    void throwsResourceNotFoundExceptionWhenGivenAnInvalidTutorialId() {
        // Arrange
        Long tutorialId = 4L;
        doThrow(ResourceNotFoundException.class).when(tutorialRepositoryMock).existsById(tutorialId);

        // Act + Assert
        assertThrows(ResourceNotFoundException.class, () -> commentService.getCommentsByTutorialId(tutorialId));
        verifyNoInteractions(commentRepositoryMock);
    }

    // Method: getCommentById()
    @Test
    @DisplayName("Returns a comment given a correct comment ID")
    void returnsCommentGivenAnId() {
        // Arrange
        Long commentId = 1L;
        when(commentRepositoryMock.findById(commentId)).thenReturn(Optional.of(firstCommentTutoOne));

        // Act
        Optional<Comment> commentFound = Optional.of(commentService.getCommentById(commentId));

        // Assert
        assertTrue(commentFound.isPresent());
        assertEquals(firstCommentTutoOne, commentFound.get());
        verify(commentRepositoryMock, times(1)).findById(commentId);
    }


    @Test
    @DisplayName("Throws ResourceNotFoundException when given an invalid comment ID")
    void throwsExceptionWhenGivenAnInvalidId() {
        // Arrange
        Long commentId = 5L;
        when(commentRepositoryMock.findById(commentId)).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(ResourceNotFoundException.class, () -> commentService.getCommentById(commentId));
    }

    // Method: createComment()
    @Test
    @DisplayName("Creates a comment successfully and returns it")
    void createsCommentSuccessfully() {
        // Arrange
        Long tutorialId = 2L;
        Comment newComment = new Comment("This is the content of the new comment", secondTutorial);

        when(tutorialRepositoryMock.findById(tutorialId)).thenReturn(Optional.of(secondTutorial));
        when(commentRepositoryMock.save(newComment)).thenReturn(newComment);

        // Act
        Comment commentCreated = commentService.createComment(tutorialId, newComment);

        // Assert
        assertEquals(newComment, commentCreated);
        verify(tutorialRepositoryMock, times(1)).findById(tutorialId);
        verify(commentRepositoryMock, times(1)).save(newComment);
    }

    @Test
    @DisplayName("Throws ResourceNotFoundException given an incorrect tutorial ID and doesnt create the comment")
    void doesntCreateCommentAndThrowsResourceNotFoundException() {
        // Arrange
        Long tutorialId = 8L;
        Comment newComment = new Comment("This is the content of the new comment", secondTutorial);

        when(tutorialRepositoryMock.findById(tutorialId)).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(ResourceNotFoundException.class, () -> commentService.createComment(tutorialId, newComment));
    }

    // Method: updateComment()
    @Test
    @DisplayName("Updates the content of the comment successfully and returns the updated resource")
    void updatesContentOfCommentSuccessfully() {
        // Arrange
        Long commentId = 1L;
        Comment updatedContentOfComment = new Comment("The content of this comment has been updated", firstTutorial);

        when(commentRepositoryMock.findById(commentId)).thenReturn(Optional.of(firstCommentTutoOne));
        when(commentRepositoryMock.save(firstCommentTutoOne)).thenReturn(firstCommentTutoOne);

        // Act
        Comment updatedComment = commentService.updateComment(commentId, updatedContentOfComment);

        // Assert
        assertNotNull(updatedComment);
        assertEquals(updatedContentOfComment.getContent(), updatedComment.getContent());
        verify(commentRepositoryMock, times(1)).findById(commentId);
        verify(commentRepositoryMock, times(1)).save(firstCommentTutoOne);
    }

    @Test
    @DisplayName("Throws ResourceNotFoundException given an incorrect comment ID")
    void doesntUpdateAndThrowsResourceNotFoundExceptionGivenAnIncorrectCommentId() {
        // Arrange
        Long commentId = 7L;
        Comment updatedContentOfComment = new Comment("The content of this comment has been updated", firstTutorial);

        when(commentRepositoryMock.findById(commentId)).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(ResourceNotFoundException.class, () -> commentService.updateComment(commentId, updatedContentOfComment));
    }

    // Method: deleteComment()
    @Test
    @DisplayName("Given a correct ID, the comment is deleted successfully")
    void commentDeletedSuccessfully() {
        // Arrange
        Long commentId = 3L;

        // Act
        commentService.deleteComment(commentId);

        // Assert
        verify(commentRepositoryMock, times(1)).deleteById(commentId);
    }

    @Test
    @DisplayName("Given an incorrect comment ID, EmptyResultDataAccessException is thrown")
    void exceptionThrownGivenAnIncorrectCommentIdToDelete() {
        // Arrange
        Long commentId = 7L;
        doThrow(EmptyResultDataAccessException.class).when(commentRepositoryMock).deleteById(commentId);

        // Act + Assert
        assertThrows(EmptyResultDataAccessException.class, () -> commentService.deleteComment(commentId));
    }

    // Method: deleteAllCommentsOfTutorial()
    @Test
    @DisplayName("Deletes all the comments from the tutorial that corresponds to the ID given")
    void deletesAllCommentsFromTutorialSuccessfully() {
        // Arrange
        Long tutorialId = 1L;
        when(tutorialRepositoryMock.existsById(tutorialId)).thenReturn(true);

        // Act
        commentService.deleteAllCommentsOfTutorial(tutorialId);

        // Assert
        verify(commentRepositoryMock, times(1)).deleteByTutorialId(tutorialId);
    }
}