package com.nttdata.restapitutorial.security.service;

import com.nttdata.restapitutorial.model.AppUser;
import com.nttdata.restapitutorial.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementación del servicio UserDetailsService; su único método permite retornar un usuario,
 * a través de la búsqueda por parámetro.
 * @author fbustosp
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AppUserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = userRepository.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("No user found with username: " + username)
        );

        return UserDetailsImpl.build(user);
    }
}
