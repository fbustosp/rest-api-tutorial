package com.nttdata.restapitutorial.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

/**
 * Manejo de excepciones que pudiesen ocurrir durante la ejecución.
 * @author fbustosp
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger logger = LogManager.getLogger(ControllerExceptionHandler.class);

    /**
     * ResourceNotFoundException (404 not found).
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorMessage> resourceNotFoundExceptionHandler(ResourceNotFoundException ex, WebRequest request) {
        ErrorMessage msg = new ErrorMessage(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false)
        );

        logger.error("No se ha encontrado un recurso asociado a la ID ingresada: {}", ex.getMessage());
        return new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
    }

    /**
     * Argumentos no válidos.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex) {
        ErrorMessage msg = new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                "Argumento no válido",
                ex.getMessage()
        );

        logger.error("Uno de los argumentos ingresados no es válido: {}", ex.getMessage());
        return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
    }

    /**
     * No se detecta el cuerpo de la petición.
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorMessage> HttpMessageNotReadableExceptionHandler(HttpMessageNotReadableException ex) {
        ErrorMessage msg = new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                "No se ha detectado el cuerpo de la petición",
                ex.getMessage()
        );

        logger.error("No se ha detectado el cuerpo de la petición: {}", ex.getMessage());
        return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
    }

    /**
     * Al solicitar un recurso con ID inexistente en la BD
     */
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ErrorMessage> emptyResultDataAccessExceptionHandler(EmptyResultDataAccessException ex) {
        ErrorMessage msg = new ErrorMessage(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                "Error al solicitar el recurso",
                ex.getMessage()
        );

        logger.error("Error al solicitar el recurso: {}", ex.getMessage());
        return new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
    }
}
