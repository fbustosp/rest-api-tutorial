package com.nttdata.restapitutorial.service.impl;

import com.nttdata.restapitutorial.exception.ResourceNotFoundException;
import com.nttdata.restapitutorial.model.Comment;
import com.nttdata.restapitutorial.repository.CommentRepository;
import com.nttdata.restapitutorial.repository.TutorialRepository;
import com.nttdata.restapitutorial.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements ICommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private TutorialRepository tutorialRepository;

    @Override
    public List<Comment> getCommentsByTutorialId(Long tutorialId) {
        if (!tutorialRepository.existsById(tutorialId)) throw new ResourceNotFoundException("No tutorial found with ID = " + tutorialId);

        return commentRepository.findByTutorialId(tutorialId);
    }

    @Override
    public Comment getCommentById(Long commentId) {
        return commentRepository.findById(commentId).orElseThrow(
                () -> new ResourceNotFoundException("No comment found with ID = " + commentId)
        );
    }

    @Override
    public Comment createComment(Long tutorialId, Comment comment) {
        return tutorialRepository.findById(tutorialId).map(
                tutorial -> {
                    comment.setTutorial(tutorial);
                    return commentRepository.save(comment);
                })
                .orElseThrow(
                        () -> new ResourceNotFoundException("No tutorial found with ID = " + tutorialId)
                );
    }

    @Override
    public Comment updateComment(Long commentId, Comment comment) {
        Comment commentToUpdate = commentRepository.findById(commentId).orElseThrow(
                () -> new ResourceNotFoundException("No comment found with ID = " + commentId)
        );
        commentToUpdate.setContent(comment.getContent());
        commentRepository.save(commentToUpdate);

        return commentToUpdate;
    }

    @Override
    public void deleteComment(Long commentId) {
        commentRepository.deleteById(commentId);
    }

    @Override
    public void deleteAllCommentsOfTutorial(Long tutorialId) {
        if (!tutorialRepository.existsById(tutorialId)) throw new ResourceNotFoundException("No tutorial found with ID = " + tutorialId);

        commentRepository.deleteByTutorialId(tutorialId);
    }
}
