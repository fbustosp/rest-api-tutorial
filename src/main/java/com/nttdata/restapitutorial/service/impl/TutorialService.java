package com.nttdata.restapitutorial.service.impl;

import com.nttdata.restapitutorial.exception.ResourceNotFoundException;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.repository.TutorialRepository;
import com.nttdata.restapitutorial.service.ITutorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TutorialService implements ITutorialService {

    @Autowired
    private TutorialRepository tutorialRepository;

    @Override
    public List<Tutorial> getAllTutorials() {
        return tutorialRepository.findAll();
    }

    @Override
    public List<Tutorial> findByTitleContaining(String title) {
        return tutorialRepository.findByTitleContaining(title);
    }

    @Override
    public Tutorial getTutorialById(Long tutorialId) {
        return tutorialRepository.findById(tutorialId).orElseThrow(
                () -> new ResourceNotFoundException("No tutorial found with ID: " + tutorialId)
        );
    }

    @Override
    public Tutorial createTutorial(Tutorial tutorial) {
        return tutorialRepository.save(tutorial);
    }

    @Override
    public Tutorial updateTutorial(Long tutorialId, Tutorial tutorial) {
        Tutorial tutorialToUpdate = this.getTutorialById(tutorialId);

        tutorialToUpdate.setTitle(tutorial.getTitle());
        tutorialToUpdate.setDescription(tutorial.getDescription());
        tutorialToUpdate.setPublished(tutorial.isPublished());

        return tutorialRepository.save(tutorialToUpdate);
    }

    @Override
    public void deleteTutorial(Long tutorialId) {
        tutorialRepository.deleteById(tutorialId);
    }

    @Override
    public void deleteAllTutorials() {
        tutorialRepository.deleteAll();
    }

    @Override
    public List<Tutorial> findByPublished() {
        return tutorialRepository.findByPublished(true);
    }
}
