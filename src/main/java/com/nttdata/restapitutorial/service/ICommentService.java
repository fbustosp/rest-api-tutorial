package com.nttdata.restapitutorial.service;

import com.nttdata.restapitutorial.model.Comment;

import java.util.List;

public interface ICommentService {

    List<Comment> getCommentsByTutorialId(Long tutorialId);

    Comment getCommentById(Long commentId);

    Comment createComment(Long tutorialId, Comment comment);

    Comment updateComment(Long commentId, Comment comment);

    void deleteComment(Long commentId);

    void deleteAllCommentsOfTutorial(Long tutorialId);
}
