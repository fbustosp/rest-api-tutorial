package com.nttdata.restapitutorial.service;

import com.nttdata.restapitutorial.model.Tutorial;

import java.util.List;

public interface ITutorialService {

    List<Tutorial> getAllTutorials();

    List<Tutorial> findByTitleContaining(String title);

    Tutorial getTutorialById(Long tutorialId);

    Tutorial createTutorial(Tutorial tutorial);

    Tutorial updateTutorial(Long tutorialId, Tutorial tutorial);

    void deleteTutorial(Long tutorialId);

    void deleteAllTutorials();

    List<Tutorial> findByPublished();
}
