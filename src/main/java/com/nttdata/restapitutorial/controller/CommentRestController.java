package com.nttdata.restapitutorial.controller;

import com.nttdata.restapitutorial.model.Comment;
import com.nttdata.restapitutorial.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class CommentRestController {

    @Autowired
    private ICommentService commentService;

    /** Obtener todos los comentarios por ID de tutorial */
    @GetMapping("/tutorials/{tutorial_id}/comments")
    public ResponseEntity<List<Comment>> getCommentsByTutorialId(@PathVariable("tutorial_id") Long tutorialId) {
        List<Comment> comments = commentService.getCommentsByTutorialId(tutorialId);

        if (comments.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        else
            return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    /** Obtener un comentario por ID */
    @GetMapping("/comments/{comment_id}")
    public ResponseEntity<Comment> getCommentById(@PathVariable("comment_id") Long commentId) {
        Comment comment = commentService.getCommentById(commentId);
        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

    /** Crear un comentario en un tutorial */
    @PostMapping("/tutorials/{tutorial_id}/comments")
    public ResponseEntity<Comment> createComment(@PathVariable("tutorial_id") Long tutorialId, @Valid @RequestBody Comment comment) {
        Comment commentCreated = commentService.createComment(tutorialId, comment);
        return new ResponseEntity<>(commentCreated, HttpStatus.CREATED);
    }

    /** Actualizar un comentario */
    @PutMapping("/comments/{comment_id}")
    public ResponseEntity<Comment> updateComment(@PathVariable("comment_id") Long commentId, @Valid @RequestBody Comment comment) {
        Comment updatedComment = commentService.updateComment(commentId, comment);
        return new ResponseEntity<>(updatedComment, HttpStatus.ACCEPTED);
    }

    /** Eliminar un comentario */
    @DeleteMapping("/comments/{comment_id}")
    public ResponseEntity<HttpStatus> deleteComment(@PathVariable("comment_id") Long commentId) {
        commentService.deleteComment(commentId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /** Eliminar todos los comentarios de un tutorial */
    @DeleteMapping("/tutorials/{tutorial_id}/comments")
    public ResponseEntity<HttpStatus> deleteAllCommentsOfTutorial(@PathVariable("tutorial_id") Long tutorialId) {
        commentService.deleteAllCommentsOfTutorial(tutorialId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
