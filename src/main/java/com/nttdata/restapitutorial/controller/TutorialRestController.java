package com.nttdata.restapitutorial.controller;

import com.nttdata.restapitutorial.exception.ResourceNotFoundException;
import com.nttdata.restapitutorial.model.Tutorial;
import com.nttdata.restapitutorial.repository.TutorialRepository;
import com.nttdata.restapitutorial.service.ITutorialService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class TutorialRestController {

    @Autowired
    private ITutorialService tutorialService;

    private static final Logger logger = LogManager.getLogger(TutorialRestController.class);

    /** Retornar todos los tutoriales */
    @GetMapping("/tutorials")
    public ResponseEntity<List<Tutorial>> getAllTutorials (@RequestParam(required = false) String title) {
        List<Tutorial> tutorials = new ArrayList<>();

        /** Si no hay parámetro, se retornan todos los tutoriales. De lo contratio, se retornan de acuerdo con el parámetro obtenido */
        if (title == null) tutorialService.getAllTutorials().forEach(tutorials::add);
        else
            tutorialService.findByTitleContaining(title).forEach(tutorials::add);

        /** Si no existen tutoriales */
        if (tutorials.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<>(tutorials, HttpStatus.OK);
    }

    /** Retornar un tutorial */
    @GetMapping("/tutorials/{tutorial_id}")
    public ResponseEntity<Tutorial> getTutorialById(@PathVariable("tutorial_id") Long tutorialId) {
        Tutorial tutorial = tutorialService.getTutorialById(tutorialId);
        return new ResponseEntity<>(tutorial, HttpStatus.OK);
    }

    /** Crear un tutorial */
    @PostMapping("/tutorials")
    public ResponseEntity<Tutorial> createTutorial(@Valid @RequestBody Tutorial tutorial) {
        Tutorial savedTutorial = tutorialService.createTutorial(tutorial);
        return new ResponseEntity<>(savedTutorial, HttpStatus.CREATED);
    }

    /** Actualizar un tutorial */
    @PutMapping("/tutorials/{tutorial_id}")
    public ResponseEntity<Tutorial> updateTutorial(@PathVariable("tutorial_id") Long tutorialId, @Valid @RequestBody Tutorial tutorial) {
        Tutorial updatedTutorial = tutorialService.updateTutorial(tutorialId, tutorial);
        return new ResponseEntity<>(updatedTutorial, HttpStatus.ACCEPTED);
    }

    /** Eliminar un tutorial */
    @DeleteMapping("tutorials/{tutorial_id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("tutorial_id") Long tutorialId) {
        tutorialService.deleteTutorial(tutorialId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /** Eliminar todos los tutoriales */
    @DeleteMapping("tutorials")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        try {
            tutorialService.deleteAllTutorials();
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /** Retornar los tutoriales que han sido publicados */
    @GetMapping("/tutorials/published")
    public ResponseEntity<List<Tutorial>> findByPublished() {
        try {
            List<Tutorial> tutorials = tutorialService.findByPublished();

            if (tutorials.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(tutorials, HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
