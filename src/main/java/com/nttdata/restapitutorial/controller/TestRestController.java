package com.nttdata.restapitutorial.controller;

import com.nttdata.restapitutorial.security.payload.response.MessageResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador para probar la securitización implementada.
 * @author fbustosp
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestRestController {

    @GetMapping("/all")
    ResponseEntity<MessageResponse> allAccess() {
        return new ResponseEntity<>(new MessageResponse("Public content"), HttpStatus.OK);
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    ResponseEntity<MessageResponse> userAccess() {
        return new ResponseEntity<>(new MessageResponse("User content"), HttpStatus.OK);
    }

    @GetMapping("/mod")
    @PreAuthorize("hasRole('MODERATOR')")
    ResponseEntity<MessageResponse> modAccess() {
        return new ResponseEntity<>(new MessageResponse("Moderator content"), HttpStatus.OK);
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity<MessageResponse> adminAccess() {
        return new ResponseEntity<>(new MessageResponse("Admin content"), HttpStatus.OK);
    }
}
